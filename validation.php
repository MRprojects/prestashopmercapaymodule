<?php
/**
     * DISCLAIMER
     *
     * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
     * versions in the future. You cannot modify and resell any part of the software you bought.
     *
     *  @author    MR <mail>
     *  @copyright 2012-2015 MR
     *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');
include(dirname(__FILE__).'/mercapay.php');

$mercapay = new mercapay();
$context = Context::getContext();
$cart = $context->cart;

if ($cart->id_customer == 0 or $cart->id_address_delivery == 0 or $cart->id_address_invoice == 0 or !$mercapay->active)
    {
     Tools::redirectLink(__PS_BASE_URI__.'order.php?step=1');
    }
// Check that this payment option is still available in case the customer changed his address just before the end of the checkout process
$authorized = false;
foreach (Module::getPaymentModules() as $module)
    {
	if ($module['name'] == 'mercapay')
	{
		$authorized = true;
		break;
	}
    }
if (!$authorized)
    {
	die(Tools::displayError('This payment method is not available.'));
    }
$customer = new Customer((int)$cart->id_customer);

if (!Validate::isLoadedObject($customer))
    {
	Tools::redirectLink(__PS_BASE_URI__.'order.php?step=1');
    }
//$currency = new Currency($cookie->id_currency);
$currency = $context->currency;
$total = (float)$cart->getOrderTotal(true, Cart::BOTH);

$configuration_array = unserialize(Configuration::get('PS_MOD_MERCAPAY'));

if ($configuration_array['idc']=='' or $configuration_array['path']=='' or $configuration_array['exe']=='')
    {
    die(Tools::displayError('Module non configuré. Merci de contacter un administrateur.'));
    }
    $parm="merchant_id=".$configuration_array['idc'];
//	$parm="merchant_id=082584341411111";

	$parm="$parm merchant_country=fr";
	$parm="$parm amount=".$total*100;
    //code monnaie : euro
	$parm="$parm currency_code=978";

    $parm="$parm pathfile=".$configuration_array['path'];

	$parm="$parm customer_id=".$cart->id_customer;
	$parm="$parm order_id=".$cart->id;

    $path_bin = $configuration_array['exe'];

	$result=exec("$path_bin $parm");


	//	sortie de la fonction : $result=!code!error!buffer!
	//	    - code=0	: la fonction génère une page html contenue dans la variable buffer
	//	    - code=-1 	: La fonction retourne un message d'erreur dans la variable error

	//On separe les differents champs et on les met dans une variable tableau

	$tableau = explode ("!", "$result");

	//	récupération des paramètres

	$code = $tableau[1];
	$error = $tableau[2];
	$message = $tableau[3];

	echo '<h2>MERCANET - Paiement Securise sur Internet</h2>';
	$texte="<br/>";
		//  analyse du code retour

  if (( $code == "" ) && ( $error == "" ) )
 	{
  	$texte.="<BR><CENTER>erreur appel request</CENTER><BR>";
  	$texte.="executable request non trouve ".$path_bin;
    //$texte.=" et path =".$configuration_array['path']." idc ".$configuration_array['idc']." executable ".$configuration_array['exe'];
	$txt = $mercapay->l("$texte");
	$mercapay->smarty->assign('messageSmarty', $txt ); // creation of our variable
	$mercapay->smarty->display(dirname(__FILE__).'/views/templates/front/validation.tpl');
 	}

	//	Erreur, affiche le message d'erreur

	else if ($code != 0){
		$texte.="<center><b><h2>Erreur appel API de paiement.</h2></center></b>";
		$texte.="<br><br><br>";
		$texte.=" message erreur : ".$error." <br>";
		$txt = $mercapay->l("$texte");
		$mercapay->smarty->assign('messageSmarty', $txt ); // creation of our variable
		$mercapay->smarty->display(dirname(__FILE__).'/views/templates/front/validation.tpl');
	}

	//	OK, affiche le formulaire HTML
	else {
		echo "<br><br>";

		# OK, affichage du mode DEBUG si activé
		echo $error."<br>";

		echo $message."<br>";
	}


include('../../footer.php');

?>

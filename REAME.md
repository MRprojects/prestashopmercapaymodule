## Prestashop online payment plugin for BNP Paribas

PHP plugin for Prestashop open source e-commerce platform. It enables online payment for online shops using BNP Paribas online services.

### Module paiement en ligne Mercapay

Ce module vous permet de proposer le paiement en ligne sécurisé par Merc@net, sous réserve d'avoir souscrit un contrat vous permettant de le faire.
Celui ci permet, une fois la procédure fournie par Merc@net suivie pour l'installation des certificats et fichiers exécutables, d'ajouter le paiement par carte bancaire à la boutique. Il faut renseigner le numéro client fourni par Merc@net ainsi que les chemins du pathfile et de l'executable request. Une fois le paiement fait, la commande est ajoutée dans le compte client.


![config1](./pic1.png)

Il faut suivre la procédure fournie par Merc@net, configurer fichiers et certificats. Ceci n'est pas compris dans le module.
Ensuite installer le module et lui donner le numéro client, le chemin du pathfile et le chemin de l'executable request.

![config2](./pic2.png)

![config3](./pic3.png)


*March 2012*

<?php
/**
     * DISCLAIMER
     *
     * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
     * versions in the future. You cannot modify and resell any part of the software you bought.
     *
     *  @author    MR <mail>
     *  @copyright 2012-2015 MR
     *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

$useSSL = true;
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');
include(dirname(__FILE__).'/mercapay.php');

$mercapay = new mercapay();
$context = Context::getContext();

//if (!$cookie->isLogged(true))
if (!$context->customer->isLogged())
    Tools::redirect('authentication.php?back=order.php');
elseif (!Customer::getAddressesTotalById((int)($context->customer->id_customer)))
	Tools::redirect('address.php?back=order.php?step=1');

$cart = new Cart($context->cookie->id_cart);
echo $mercapay->execPayment($cart);
include_once(dirname(__FILE__).'/../../footer.php');

?>

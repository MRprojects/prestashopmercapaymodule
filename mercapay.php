<?php
/**
    * DISCLAIMER
    *
    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
    * versions in the future. You cannot modify and resell any part of the software you bought.
    *
    *  @author    MR <mail>
    *  @copyright 2012-2015 MR
    *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

if (!defined('_PS_VERSION_'))
  exit;

class mercapay extends PaymentModule
  {
	public function __construct()
	    {
	    $this->name = 'mercapay';
	    $this->tab = 'payments_gateways';
	    $this->version = '1.1.0';
	    $this->author = 'MR';
	    $this->need_instance = 0;

	    parent::__construct();
        $this->module_key="5abe6c57a6e0739c9f2dad46c2e3c901";
        if (!Configuration::get('PS_MOD_MERCAPAY'))
            $this->warning = $this->l('No name provided');

        $this->displayName = $this->l('Paiement MERC@NET');
	    $this->description = $this->l('Module de paiement sécurisé avec MERC@NET');

        }

	public function install()
		{
            if (!parent::install() OR !$this->registerHook('payment') OR !$this->registerHook('paymentReturn') OR !Configuration::updateValue('PS_MOD_MERCAPAY', serialize(array('idc'=>'','path'=>'','exe'=>''))))
                return false;
			return true;
		}

	public function uninstall()
		{
		if (!parent::uninstall())
		    Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'mercapay`');
        return (Configuration::deleteByName('PS_MOD_MERCAPAY') AND parent::uninstall());
		}


      public function getContent()
      {
          $configuration_array = unserialize(Configuration::get('PS_MOD_MERCAPAY'));
          // If we try to update the settings
          //if (isset($_POST['submitModule']))
          if(Tools::getIsset(Tools::getValue('submitModule')))
          {
              $configuration_array['idc']=(preg_match('/^[0-9]+/', Tools::getValue('idc')) ? Tools::getValue('idc'): '');
              $configuration_array['path']=((Tools::getValue('path') != '') ? Tools::getValue('path'): '');
              $configuration_array['exe']=((Tools::getValue('exe') != '') ? Tools::getValue('exe'): '');


              Configuration::updateValue('PS_MOD_MERCAPAY', serialize($configuration_array));
              echo '<div class="conf confirm">'.$this->l('Configuration updated').'</div>';
          }

          return '
          <h2>'.$this->displayName.'</h2>
          <form action="'.Tools::htmlentitiesutf8($_SERVER['REQUEST_URI']).'" method="post">
          <fieldset class="width2">
          <label for="idc">'.$this->l('Identifiant Merc@net : ').'</label>
              <input type="text" id="idc" name="idc" value="'.(($configuration_array['idc'] != "") ? $configuration_array['idc'] : "").'" />
              <div class="clear">&nbsp;</div>
          <label for="path">'.$this->l('Adresse du fichier pathfile (exemple : c:/mondossier/pathfile): ').'</label>
              <input type="text" id="path" name="path" value="'.(($configuration_array['path'] != "") ? $configuration_array['path'] : "").'" />
              <div class="clear">&nbsp;</div>
          <label for="exe">'.$this->l('Adresse de l\'executable request (exemple : c:/mondossier/request.exe) : ').'</label>
              <input type="text" id="exe" name="exe" value="'.(($configuration_array['exe'] != "") ? $configuration_array['exe'] : "").'" />
              <div class="clear">&nbsp;</div>
          <br /><center><input type="submit" name="submitModule" value="'.$this->l('Update settings').'" class="button" /></center>
          </fieldset>
          </form>
          <div class="clear">&nbsp;</div>
          <fieldset>
          <legend>Addons</legend>
          '.$this->l('This module has been developped by MR and can only be sold through').' <a href="http://addons.prestashop.com">addons.prestashop.com</a>.<br />
          '.$this->l('Please report all bugs to').' <a href="mailto:hello@mail.com">hello(at)mail(dot)com</a></fieldset>';
      }





	public function execPayment($cart)
		{
		if (!$this->active)
			return ;
		if (!$this->_checkCurrency($cart))
			Tools::redirectLink(__PS_BASE_URI__.'order.php');

		$this->smarty->assign(array(
			'nbProducts' => $cart->nbProducts(),
			'cust_currency' => $cart->id_currency,
			'currencies' => $this->getCurrency((int)$cart->id_currency),
			'total' => $cart->getOrderTotal(true, Cart::BOTH),
			'this_path' => $this->_path,
			'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->name.'/'
		));

		return $this->display(__FILE__, '/views/templates/front/payment_execution.tpl');
		}

	public function hookPayment($params)
		{
		if (!$this->active)
			return ;
		if (!$this->_checkCurrency($params['cart']))
			return ;

		$this->smarty->assign(array(
			'this_path' => $this->_path,
			'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->name.'/'
		));
		return $this->display(__FILE__, '/views/templates/front/payment.tpl');
		}

	public function hookPaymentReturn($params)
		{
		if (!$this->active)
			return ;

		$state = $params['objOrder']->getCurrentState();
		if ($state == Configuration::get('PS_OS_PAYMENT') OR $state == Configuration::get('PS_OS_OUTOFSTOCK'))
			$this->smarty->assign(array(
				'total_to_pay' => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
	//			'bankwireDetails' => nl2br2($this->details),
	//			'bankwireAddress' => nl2br2($this->address),
	//			'bankwireOwner' => $this->owner,
				'status' => 'ok',
				'id_order' => $params['objOrder']->id
			));
		else
			$this->smarty->assign('status', 'failed');
		return $this->display(__FILE__, '/views/templates/front/payment_return.tpl');
		}

  	private function _checkCurrency($cart)
	{
		$currency_order = new Currency((int)($cart->id_currency));
		$currencies_module = $this->getCurrency((int)$cart->id_currency);
		//$currency_default = Configuration::get('PS_CURRENCY_DEFAULT');

		if (is_array($currencies_module))
			foreach ($currencies_module AS $currency_module)
				if ($currency_order->id == $currency_module['id_currency'])
					return true;
		return false;
	}
  }
?>
